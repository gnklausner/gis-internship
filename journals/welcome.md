## New Hire Guide ##

Welcome! We're all really excited for the new semester and having you on board!

This is a living document. It will change over time, and will greatly benefit from your contributions.

### Day 0 ###
Let's get you spun up with the basics.

#### Mac ####
1. It'll take a a while, so get Xcode downloading and installing. You can find this in the Mac app store.
1. If you have a preference of text editors/IDEs, please feel free to use that. However, we use Visual Studio Code and rely on its features for remote pairing.
  Useful extensions:
    - GitLens
    - indent-rainbow
    - Live Share
    - Python
    - One Dark Pro
    - Ruby
1. Make a GitLab account
     - Generate [SSH keys](https://docs.gitlab.com/ee/ssh/) and give GitLab your public key
     - Clone this [repo](https://gitlab.com/gnklausner/gis-internship)
     - Move the [Welcome ticket](https://gitlab.com/gnklausner/gis-internship/-/boards) to "To Do"
1. Generate a directory for journals
   - Open your terminal
   - Within the repo, `mkdir journals`
   - `touch yyyy_mm_dd.md`, where you replace the dates with today's date
   - `git add journals/yyyy_mm_dd.md`
   - `git commit -m 'Your message here'`
   - `git push`
1. Sign up for and install LastPass browser extension (free)
1. Sign up and install Discord
1. Get ebooks from @gnklausner

