#return value: value a function returns
#the return statement takes a value from inside a function and sends it back to the line that called the functions
#returning a simple value
def get_formatted_name(first_name, last_name):
    """return a full name, neatly formatted"""
    full_name = f"{first_name} {last_name}"
    return full_name.title()
musician = get_formatted_name('jimi', 'hendrix')
print(musician)

#Making an argument optional in case user wants to put extra information
def get_formatted_name(first_name, middle_name, last_name):
    """return a full name, neatly formatted."""
    full_name = f"{first_name} {middle_name} {last_name}"
    return full_name.title()
musician = get_formatted_name('john', 'lee', 'hooker')
print(musician)

def get_formatted_name(first_name, last_name, middle_name=''):
    if middle_name:
        full_name = f"{first_name} {middle_name} {last_name}"
    else:
        full_name = f"{first_name} {last_name}
musician = get_formatted_name('abbie', 'dodson')"
print(musician)
musician = get_formatted_name('abbie', 'lyndsey', 'dodson')
print(musician)

#8-6
def city_country(name_city, name_country):
    the_place = f"{name_city} {name_country}"
    return the_place.title
location = ('richmond', 'virginia')

