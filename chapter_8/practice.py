def display_message():
    print("I am learning about functions.")
display_message()

def favorite_book(title):
    print(f"My favorite book is {title}")
favorite_book('Harry Potter')

def make_shirt(message, shirt_size='large'):
    print(f"My shirt size is {shirt_size}. \nI want my shirt to say \"{message}\".")
make_shirt('hello')

def city_country(city, country):
    return (f"{city}, {country}")
place = city_country('richmond', 'united states')
print(place)

MENU = {
    'coffee': 2.00,
    'muffin': 3.00,
    'chai': 3.49
}
def price(item):
    return MENU[item]
item_price = price('coffee')

def get_taxed(pretax):
    return pretax * 1.05

final_price = get_taxed(item_price)
print(final_price)

#practice from G
def choose_bread(bread = 'white'):
    return [bread, bread]

def add_fillings(bread, fillings):
    # fillings = ['ham', 'cheese']
    fillings.insert(0, bread[0])
    fillings.append(bread[1])
    return fillings

def toaster(we_want_toast, sandwich):
    #define sandwich as 0 or 1 or something like that and then start and stop
    if we_want_toast:
        sandwich[0] = 'toasted ' + sandwich[0]
        sandwich[-1] = 'toasted ' + sandwich[-1]
        return sandwich 
    else:
        return sandwich

bread = choose_bread('wheat')
print(bread)
sandwich = add_fillings(bread, ['ham', 'cheese', 'pickles'])
print(sandwich)
toasted_sandwich = toaster(True, sandwich) # [white, ham, white] => [toasted white, ham, toasted white]
print(toasted_sandwich)

def f(x, h):
    return x * h

y = f(4, 3)
print(y)

def squareNumber(num):
    squared = num ** 2
    return squared

four_squared = squareNumber(4)
again = squareNumber(four_squared)
three_squared = squareNumber(3)
three_to_the_fourth = squareNumber(three_squared)
print(four_squared, again, three_squared, three_to_the_fourth)

def math(addition):
    addition = x + y

def add(a, b):
    return a + b
    add(7, 3)

def celsius (f):
  return (((32*f)-32)*(5/9))
celsius(32)

def celsius (f):
  return ((f-32)*(5/9))
celsius(32)
def kelvin(celsius)
  return celsius + 273.15
kelvin(0)

def celsius (f):
  return ((f-32)*(5/9))

def kelvin(celsius):
  return celsius + 273.15
kelvin(0)

def fahrenheit_to_celsius(f):
  return (f-32) * (5/9)

def fahrenheit_to_kelvin(f):
  return fahrenheit_to_celsius(f) + 273.15

print(fahrenheit_to_celsius(212)) # 100
print(fahrenheit_to_kelvin(212))