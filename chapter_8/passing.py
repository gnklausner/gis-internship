#passing a list
def greet_users(names):
    """Print a simple greeting to each user in the list."""
    for name in names:
        msg = f"hello, {name.title()}!"
        print(msg)
usernames = ['abbie', 'g', 'mrs. ownes']
greet_users(usernames)
