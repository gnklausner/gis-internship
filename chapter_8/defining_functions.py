#functions are named blocks of code that are designed to do one specific job
def greet_user():
    """Display a simple greeting."""
    print("hello!")
    #print(f"hello, {username.title()}!")
greet_user()
#greet_user('jesse')

#8-1
def display_message():
    print("This chapter, I am learning about functions!")
display_message()

#8-2
def favorite_book():
    print(f"My favorite book is {}.")
favorite_book('alice in wonderland')

#passing arguments
#position arguments: need to be in the same order the parameters were written. 
#Python must match each function call with a parameter ^^
def describe_pet(animal_type, pet_name)"
    """Display information about a pet."""
    print(f"\nI have a {animal_type}.")
    print(f"MY {animal_type}'s name is {pet_name.title()}.'")
describe_pet('dog', 'Yuki')

#multiple fuction calls
#just add another description
describe_pet('dog', 'frazier')

#keyword arguments: a name-value pair that you pass to a function
describe_pet(animal_type= 'dog', pet_name= 'theo')

#default values: if an argument for a parameter is provided in the function call, python uses the argument value

#equivalent function calls: there are many ways to call a function