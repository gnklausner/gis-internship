#allows you to examine the current state of a program and respond approprately to that state
#simple example
cars = ['audi', 'bmw', 'subaru', 'toyota']
for car in cars:
    if car == 'bmw':
        print(car.upper())
    else:
        print(car.title())

#conditional statements
#can be evaluated as true or false, if true, python executes code. if false, python ignores

#checking for equality
car = 'bmw'
car == 'bmw'
#True
car = 'jeep'
car == 'truck'

#ignoring case when checking for equality
car = "Audie"
car == "aduie"
#will come out at false
#but
car = "Audie"
car.lower() == "audie"
#true

#checking for inquality
# != is does not equal

requested_icecream = 'mint chip'
if requested_icecream != 'vanilla':
    print("I don't want vanilla!")

#Numerical comparisons
answer = 20
if answer != 21:
    print("WTH DUDE WRONG ANSWER")

age = 18
age < 21
age <= 21
age >= 21

#checking multiple conditions
age_0 = 22
age_1 = 18
age_0 >= 21 and age_1 >= 21
#prints false
age_1 = 22
age_0 >= 21 and age_1 >= 21
#true

#an OR expression fails only when both individual tests fail
age_0 = 22
age_1 = 18
age_0 >= 21 or age_1 >= 21
#true
age_0 = 18
age_0 >= 21 or age_1 >= 21
#false

#checking whether a value is in a list
#like in a list of usernames, you want a username that hasn't been used, so the program will check the database for that username
requested_toppings = ['mushroom', 'onions', 'pineapple']
'mushroom' in requested_toppings
#true
'pepperoni' in requested_toppings
#false

#Boolean Expressions
#another name for a conditional test; it is neither true or false
#used to check things like whether a game is running or if a user can edit parts of a website
game_active = True
can_edit = False

#practice 5-1
ice_cream = 'mango'
print("Is my favorite icecream == 'mango'? I predice True.")
print(ice_cream == 'mango')
print("\nIs ice_cream == 'vanilla'? I predict False.")
print(ice_cream == 'vanilla')

#practice 5-2 
soccer_position == 7
soccer_positions == 9
soccer_position >= 6 and soccer_positions >= 6

brother = Avery
brother.lower() == avery 
print(brother)

classmates = ['gracie', 'emma', 'jordyn']
'chris' in classmates
'grace' in classmates