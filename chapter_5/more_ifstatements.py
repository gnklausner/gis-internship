#simple if statements
#one test and one action:
if conditional_test:
    do something

age = 19
if age >= 18:
    print("bitch you can vote so go vote")

#if statements indent like for loops
#all indented lines after an if statement will be executed if teh test passes, and teh entire block of indented lines will be ignored if the test does not pass
#could also add to last one...
    print("did you bother to register?")

#if-else statements
#allows you to define an action of set of actions that are executed when the conditional test fails
age = 17
if age >=18:
    print("bitch you can vote so go vote")
    print("did you bother to register?")
else:
    print("kiddo you are too young to vote")
    print("see ya in four years!")

#the if-elif-else Chain

#more than two possible conditions
age = 12 
if age < 4:
    print("your admission cost is $0")
elif age < 18:
    print("your admission cost is $25")
else:
    print("cost is $40")


#Using multiple elif blocks
if age < 4:
    print("your admission cost is $0")
elif age < 18:
    print("your admission cost is $25")
elif age < 65:
    price = 40
else:
    print("cost is $40")

#omitting the else block makes it more clear; each block of code must pass a specific test in order to be executed

#testing multiple conditions
requested_toppings = ['mushrooms', 'extra cheese']
if 'mushrooms' in requested_toppings:
    print("adding mushrooms.")
if 'pepperoni' in requested_toppings:
    print("adding pepperoni.")
if 'estra cheese' in requested_toppings:
    print("adding extra cheese.")
print("\nFinished making your pizza")

#5-3
red = 2
green = 1
yellow = 3
alien_color = red
if alien_color = 2:
    print("you just scored 5 points")
elif alien_color < 2:
    print("the color is not green")
elif alien_color > 2:
    print("the color is not yellow")

#Using if statements with lists
#you can watch for special values that need to be treated differently than other values in teh list; manage changing conditions efficiently

#checking for special items
requested_toppings = ['mushrooms', 'green pepers', 'extra cheese']
for requested_toppings in requested_toppings:
    print(f"adding {requested_topping}")
print("\nFinished making your pizza!")

requested_toppings = ['mushrooms', 'green pepers', 'extra cheese']
for requested_toppings in requested_toppings:
    if requested_topping == 'green peppers':
        print("sorry, we are out of green peppers right now")
    else:
        print(f"adding {requested_topping}.")
print("\nFinished making your pizza")

#checking that a list is not empty
requested_toppings = []
if requested_toppings:
    for requested_toppings:
        print(f"adding {requested_topping}.")
    print("\nFinished making your pizza")
else:
    print("are you sure you want a plain pizza")


#using multiple lists 
available_toppings = ['mushrooms', 'olives', 'green peppers', 'pepperoni', 'pineapple', 'extra cheese']
requested_toppings = ['mushrooms', 'french fries', 'extra cheese']
for requested_toppings in available_toppings:
    if requested_toppings in available_toppings:
        print(f"adding {requsted_toppings}")
    else:
        print)(f"sorry, we don't have {requested_toppigs}")