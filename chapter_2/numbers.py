#math crap
print(2 + 3)
print(3 - 2)
print(2 * 3)
print(2 / 2)
print(2 + 3*4) #follows PEMDAS; also, it keeps running my strings file and not his one and I am pissed

#floats: any number with a decimal point
print(0.1 + 0.2)
print(0.2 + 0.1)

#Integers and floats
#dividing numbers always gets you a float
print(4/2)
print(1 + 2.0)

#Underscores in numbers
universe_age = 14_000_000_000
print(universe_age)

#multiple assignment
x, y, z = 0, 0, 0

#Constants: like a variable whoose value stays the same throughout the life of the program; use capital letters
MAX_CONNECTIONS = 5000

#Practice 2-8
print(2 + 8)
print(10 - 2)
print(2*4)
print(16 / 2)

#practice 2-9
favorite_number = 20
message = f"My favorite number is {favorite_number}"
print(message)
