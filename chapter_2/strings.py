#strings are series of characters. anything in a quote set is a string. can be single ' or double " quotes
name = "abbie dodson"
print(name.title())
#title() is considered a method. Methods are actions which can be done to data. The period inbetween name.title tells python to use title as a methon
#the title () method capitalized the A and D in my name (like a title)
#if I used .upper or .lower, my name would be printed in all upper or all lower

#variables in strings

first_name = "abbie"
last_name = "dodson"
full_name = f"{first_name} {last_name}" #this is the line that doesn't work. No idea why
print(full_name)

#I made two variables into one big variable. Also, it didn't work the first time I ran it. Still can't figure out why
#oookay still wont work so moving on. f is for format in f-strings.
#I could also do print(f"hello, {full_name.title()}!) to get  "Hello, Abbie Dodson!". I can also make the f statement a message and then just say print(message)

#white space
print("\tPython")

#new line in string
print("Languages:\nPython\nC\nJavaScript")

#practce 2-3
name_eric = "Eric"
#message = (f"Hello, {name_eric.title()}")
#print(message) these lines didn't work
