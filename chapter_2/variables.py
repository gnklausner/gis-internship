message = "Hello Python world!"
print(message)
#use command shift p and then Python: run python file in terminal
#python printed my message. The name of my variable was 'message'

message = "Hello Python Crash Course world!"
print(message)
#hehe it since the variable "message" had two values, it printed both
#variable can contain the following: letters, numbers, and underscores. NO SPACES