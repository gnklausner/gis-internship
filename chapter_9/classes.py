#object oriented programming is one of the most effective approaches to writing software
#write classes that represent real-world things and situations, and you create objects based on these classes
#instantiation: making an object from a class

#Creating and using a classs

#creating dog class
class Dog:
    """A simple attempt to model a dog."""
    def_init_(self, name, age):
        """initialize name and age attributes"""
        self.name = name 
        self.age = age
        def sit(self):
            """simulate a dog sitting in response to a command."""
            print(f"{self.name} is now sitting.")

#the_init_() method
#this method has two leading underscores and two trailing underscores, a convention that helps prevent Pythons defeault method

#making an instance from a class
my_dog = Dog('willie', 6)
print(f"my dog name is {my_dog.name}")
print(f"my dog age is {my_dog.age}")

