#slicing a list
#specify the index of teh first and last elements you wan tto work with
players = ['abbie', 'morgan', 'ashlee', 'lizzie', 'jolie']
print(players[0:3])
print(players[2:])#prints after 
#looping through a slice
players = ['abbie', 'morgan', 'ashlee', 'lizzie', 'jolie']
print("Here are teh first three players on my team")
for player in players[:3]:
    print(players.title())
#copying a list
my_foods = ['pizza', 'spaghetti', 'chips', 'salsa']
friend_foods = my_foods[:]
print("My favorite foods are:")
print(my_foods)
print("\My freind's favorite foods are:")
print(friend_foods)

my_foods = ['pizza', 'spaghetti', 'chips', 'salsa']
friend_foods = my_foods[:]
my_foods.append('cannoli')
friend_foods.append('ice cream')