for value in range(1, 5):
    print(value)
#put range() in list () to go horizontal
numbers = list(range())
    print(numbers)

#hot to print even numbers 1-10
even_numbers = list(range(2, 11, 2))
    print(even_nubers)
#starts at 2 and adds 2 repeatedly
#** is exponents

squares = []
for value in range (1, 11):
    square = value**2
    squares.append(square)
print(squares)

#stats with number lists
digits = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
min(digits)
max(digits)
sum(digits)

#list comprehension allows you to generate this same list in just one line of code
squares = [value**2 for value in range(1, 11)]
print(squares)

#4-3 practice
for value in range(1, 21):
    print(value)

#4-4 practice
for value in range(1_000_000):
    print(value)
numbers = list(range(1, 1_000_000))
    print(numbers)
#try this
for value in list(range(1, 1_000_000)):
    print(value)

#4-5
for value in range(1, 1_000_000):
    print(sum(value))

numbers = list(range(1, 1_000_000))
min(numbers)
max(numbers)
sum(numbers)

#4-6
odd_numbers = [1, 20,2]
print(list(odd_numbers))
#try
for odd_numbers in range(1, 20,2):
    print(list(odd_numbers))

#4-7 practice
multiples_three = [3, 30, 3]
print(list(multiples_three))
#try
for multiples_three in range(3, 30):
    print(list(multiples_three))

#4-8 practice
cubed = []
for value in range(1, 10):
    cubed = value **3
    cubed.append(cubed)
print(list(cubed))