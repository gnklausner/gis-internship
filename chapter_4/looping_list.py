#looping will let me work with all items in a list; it is more efficient
magicians = ['alice', 'david', 'carolina']
for magician in magicians: #tells Python to retrieve first value and associate it with the variable magician
    print(magician) 

magicians = ['alice', 'david', 'carolina']
for magician in magicians: 
    print(f"{magician.title()}, that was a great trick!")
#will say same thing for all three magicians
    print(f"I can't wait to see your next trick, {magician.title()}.\n")
print("You guys all suck!")

#4-1 practice
pizzas = ['pepperoni', 'cheese', 'sausage']
for pizza in pizzas:
    print(f"I love {pizzas} pizza.")
print("I really love pizza bro")

#4-2 animals
animals = ['cat', 'dog', "axolotl"]
for animal in animals:
    print(f"A {animals} would make a great pet.")
print("LOL i want an axolotl.")

