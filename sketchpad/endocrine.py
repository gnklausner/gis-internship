import random

QUESTIONS = {
  'hypothalamus': 'regulates anterior pituitary hormones',
  'pituitary gland': 'regulates growth and various metabolic activities of other endocrine',
  'parathyroid gland': 'regulates levels of calcium and phosphate',
  'thymus': 'processes developing T and B cells',
  'testes': 'produce sperm and male sex hormones',
  'pineal gland': 'may affect anterior pituitary hormones',
  'thyroid gland': 'controls rate of metabolism',
  'adrenal glands': 'affects metabolism, blood pressure, sodium, and potassium levels',
  'pancreas': 'regulates blood glucose levels',
  'ovaries': 'produce oval and female sex hormones'
}

MAX_TRIES = 3

def main():
  # generate array of flash cards
  flash_cards = []
  for answer, question in QUESTIONS.items():
    flash_cards.append({'question': question, 'answer': answer})

  # shuffle stack of flash cards
  random.shuffle(flash_cards)

  correct_count = 0
  # prompt user for answers
  for card in flash_cards:
    try_count = 0
    correct = False
    while try_count < MAX_TRIES:
      response = input(card['question'].capitalize() + ': ')
      normalized_response = normalize(response)
      if normalized_response == normalize(card['answer']):
        print('Correct')
        correct = True
        correct_count += 1
        break
      else:
        print('Try again')
        try_count += 1
    if correct == False:
      print('The correct answer is: ' + card['answer'])

  percent_correct = (correct_count / len(flash_cards)) * 100
  print("You scored {0:.0f}%.".format(percent_correct, correct_count))

def normalize(str):
  return str.lower().strip()

main()

# class FlashCard:
#   def __init__(self, question, answer):
#     self.question = question
#     self.answer = answer

# class FlashCards:
#   def __init__(self, flash_cards = []):
#     self.flash_cards = flash_cards

#   def add_card(self, card):
#     self.flash_cards.append(card)

#   def remove_card(self, card):
#     self.flash_cards.remove(card)

#   def randomize(self):
#     self.flash_cards.
