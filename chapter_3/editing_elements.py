#modifyling elements in a list

icecreams = ['vanilla', 'chocolate', 'strawberry']
print(icecreams)
icecreams[1] = 'peach'#renamed second item
print(icecreams)

#adding elements: append an item

icecreams.append('berry')
print(icecreams)
icecreams.insert(2, 'cherry')
print(icecreams)

#removing elements from a list
family  = ['mom', 'avery', 'dad']
print(family)
del family[-1] #del is delete
print(family)

#removing using pop()
popped_family = family.pop()
print(popped)_family)

#pop out a value from list, reprint list w/o value, show we still have that value but its separate
least_favorite = family.pop(2)
print(f"my least favorite family member is {least_favorite}.")
#remove by value
family.remove('dad')
print(family)

#3-4 pracice
guests = ['Chris', 'Joe', 'Brayden']
invite = f"{guest[0]}, want to come for pizza?"
print(invite)

#3-5 practice
print("Chris cannot make it.")
guests[0] = 'Brent'
print(guests)

#3-6 practice
print("I want more people!")
guests.insert(0, 'Keegan')
guests.insert(2, 'Brandon')
guests.insert(-1, 'Avery')
print(guests)

#3-7 practice
print("I want less people!")
less_guests = guests.pop(3)
print guests