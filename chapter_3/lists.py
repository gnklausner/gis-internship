#a list is a collection of items in a particular order
# [] indicates a list

sports = ['soccer', 'basketball', 'hockey']
print(sports)

#accessing elements in a list
#index=position of an item

print(sports[0])

#start with zero

print(sports[0].title())  #capitalize

#to request last item, use [-1]
#using individual values from a list

message = f"my favorite sport is {sports[0].title()}."
print message
#3-1 practice

friends = ['Nan', 'Jolie', 'Hailey']
print(friends[0])
print(friends[1])

#3-2 practice

friends_message = f"I love you {friends[0]}!"
print(friends_message)

#3-3 practice

cars = ['jeep', 'truck']
message_jeep = f"I want a blue {cars[0].title()}!"
print(message_jeep)