#dictionaries can be used to store information in a variety of ways; therefore, several different ways exist to loop throught them
#looping through key-value pairs
user_0 = {
    'username': 'enfermi',
    'first': 'enrico',
    'last': 'fermi'
}
for key, value in user_0.items():
    print(f"\nKey: {key}")
    print(f"Value: {value}")

favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
}
for name, language in favorite_languages.items():
    print(f"{name.title()}'s favorite language is {language.title()}.'")
for name in favorite_languages.keys():
    print(name.title())

#looping through a dictionary's keys in a particular order
favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
}
for name in sorted(favorite_languages.keys()):
    print(f"{name.title()}, thank you for taking the poll.")

#looping through all values in a dictionary
favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
}
print("the following languages have been mentioned:")
for language in favorite_languages.values():
    print(language.title())

#6-5
rivers_country = {
    'nile': 'egypt',
    'mississippi': 'united states',
    'amazon',: 'somewhere in south america'
}
for name in sorted(rivers_country.keys()):
    print(f"{name.title()} river is in")