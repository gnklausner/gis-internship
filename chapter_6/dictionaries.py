#a simple dictionary
alien_0 = {'color': 'green', 'points': 5}
print(alien_0['color'])
print(alien_0['points'])

#working with dictionaries
#key-value... pair collection with in a dictionary. They are sets of values associated with each other

#accessing values in a dictionary
#use [] 

alien_0 = {'color': 'green'}
print(alien_0['color'])

alien_0 = {'color': 'green', 'points': 5}
new_points = alien_0['points']
print(f"You just earned {new_points} points!")

#adding new key-value pairs
#dictionaries are DYNAMIC STRUCTURES... this means adaptable, always changing, interactive

alien_0 = {'color': 'green', 'points': 5}
print(alien_0)
alien_0['x_position'] = 0
alien_0['y_position'] = 25
print(alien_0)
#prints {'color': 'green', 'points': 5, 'x_position': 0, 'y_position': 25}

#starting with an empty dictionary
alien_0 = {}
alien_0['color'] = 'green'
alien_0['points'] = 5
print(alien_0)

#modifying values in a dictionary
alien_0 = {'color': 'green'}
print(f"the alien is {alien_0['color']}.")
alien_0['color'] = 'yellow'
print(f"the alien is not {alien_0['color']}.")

alien_0 = {'x_position': 0, 'y_position': 25, 'speed': 'medium'}
print(f"original position: {alien_0['x_position']}")
#move the alien to the right.
##determine how far to move the alien based on its current speed
if alien_0['speed'] == 'slow':
    x_increment = 1
elif alien_0['speed'] == 'medium':
    x_increment = 2
else:
    #this must be a fst alien
    x_increment = 3
#the new position is the old position plus the increment
alien_0(f"New position: {alien_0['x_postion']}")

#removing key-value pairs
alien_0 = {'color': 'green', 'points': 5}
print(alien_0)
del alien_0['points']
print(alien_0)

#dictionary of similar objects
favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
}
language = favorite_languages['sarah'].title()
print(f"Sarah's favorite language is{language}")

favorite_languages['sarah']

#using get() to access values
alien_0 = {'color': 'green', 'speed': 'slow'}
point_value = alien_0.get('points', 'no point value assigned.')
print(point_value)

