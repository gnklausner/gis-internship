def check_guess(guess, answer):
    still_guessing = True
    attempt = 0
    while still_guessing and attempt < 3:
        if guess.lower() == answer.lower():
            print('Correct Answer')
            still_guessing = False
        else:
            if attempt < 2:
                guess = input ("That's wrong idiot")
            attempt = attempt + 1

    if attempt == 3:
        print('The correct answer is ' + answer)

print('Endocrine System Practice Quiz')
guess1 = input('regulates anterior pituitary hormones')
check_guess(guess1, 'hypothalamus')
guess2 = input('regulates growth and various metabolic activites of other endocrine')
check_guess(guess2, 'pituitary gland')
guess3 = input('regulates levels of calcium and phosphate')
check_guess(guess3, 'parathyroid gland')
guess4 = input('processes developing T and B cells')
check_guess(guess4, 'thymus')
guess5 = input('produce sperm and male sex hormones')
check_guess(guess5, 'testes')
guess6 = input("may affect anterior pituitary hormones")
check_guess(guess6, 'pineal gland')
guess7 = input('controls rate of metabolism')
check_guess(guess7, 'thyroid gland')
guess8 = input('affects metabolism, blood pressre, sodium, and potassium levels')
check_guess(guess8, 'adrenal glands')
guess9 = input('regulates blood glucose levels')
check_guess(guess9, 'pancreas')
guess10 = input('produce oval and female sex hormoves')
check_guess(guess10, 'ovaries')
